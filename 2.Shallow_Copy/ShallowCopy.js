"use strict";
function shallowCopy(student) {                                 //Function to perform shallow copy
    var student_copy = {}, i;                                   //Creating a temporary object
    for (i in student) {
        student_copy[i] = student[i];                           //Copying the properties from the parent object
    }
    return student_copy;
}

function Student(name, age, roll, marks) {                      //Creating constructor named Student
    this.name = name;
    this.age = age;
    this.roll = roll;
    this.marks = marks;
}

var student1 = new Student("vairav", 21, 146, [96, 98, 100]);   //Creating an object student1
var student2 = shallowCopy(student1);                           //Copying the content of student1 to student2 by calling the function
console.log(student1);
student2.marks[1] = 60;                                         //Checking shallow copy happens or not
student2.name = "Gokul";                            
console.log(student2);