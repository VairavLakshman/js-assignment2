function main() {
    var number = parseInt(document.getElementById("number").value);                              //Getting number from user through html 
    if (number > 255) {
        alert("The entered number is not 8 bit");                                               //Checking if the number exceeds 8 bit 
    } else {
        var base2 = number.toString(2);                                                                 //Converting number into binary array
        
        base2 = base2.split('').reverse().join('');                                                     //Reversing to get LSB in the beginning
        var startBit = parseInt(document.getElementById("start").value), i;                                 
        var stopBit = parseInt(document.getElementById("stop").value);  //Stop bit
        var rBit = document.getElementById("replace");            //Replacing value from the user
        var replaceBit = parseInt(rBit.options[rBit.selectedIndex].value);
        for (i = startBit - 1; i <= stopBit - 1; i += 1) {
            base2 = base2.replace(base2[i], replaceBit);                                             //Replacing the appropriate bits
        }
        base2 = base2.split('').reverse().join('');                                                     //Revesing back to get back the actual binary number
        var result = parseInt(base2, 2);                                                             //Converting binary array back to decimal
        alert("The resultant number is " + result);                                                                         
    }
}