"use strict";

function deepCopy(student) {                                            //Function to perform deep copy
    var copy_student = Object.create(Object.getPrototypeOf(student)), i, prop_desc, prop = Object.getOwnPropertyNames(student);
    for (i = 0; i < prop.length; i += 1) {
        prop_desc = Object.getOwnPropertyDescriptor(student, prop[i]);  //Getting parent objects descriptors and properties
        if (prop_desc.value && typeof prop_desc.value === 'object') {
            prop_desc.value = deepCopy(prop_desc.value);                //Recurively calling the function if type of variable is reference
        }
        Object.defineProperty(copy_student, prop[i], prop_desc);        //Defining the properties of newly created object
    }
    return copy_student;
}

function Student(name, age, roll, marks) {                              
    this.name = name;
    this.age = age;
    this.roll = roll;
    this.marks = marks;
}

var student1 = new Student("vairav", 21, 146, [96, 98, 100]);
var student2 = deepCopy(student1);
console.log(student1);
student2.marks[1] = 60;
student2.name = "Gokul";
console.log(student2);