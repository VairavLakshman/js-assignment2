function Vehicle(name, model) {                     //Creating a constructor named Vehicle
    this.name = name;
    this.model = model;
}

Vehicle.prototype.getName = function () {           //Defining prototype for the Vehicle constructor
    console.log("Vehicle's Name: " + this.name + "\nVehicle's Model: " + this.model);
};

var car = new Vehicle("Maruti", "Baleno");          //Creating new object car
car.engine = 1;

console.log(car);
car.getName();

function inherit(car, childName, childModel) {      //Function for Inheritance
    var vehicle = Object.create(car);               //Inheriting car's property to temporary object
    vehicle.name = childName;
    vehicle.model = childModel;
    return vehicle;
}

var bike = inherit(car, "Bajaj", "Discover");       //Creating object named bike and calling the inheritance function

bike.wheels = 2;
console.log(bike);
bike.getName();