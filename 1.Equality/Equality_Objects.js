"use strict";

function equalityCheck(object_1, object_2) {                                        //Function to check equality
    var propArr1 = [], propArr2 = [], valArr1 = [], valArr2 = [], prop1, prop2, i;
    for (prop1 in object_1) {                                                       //Pushing the properties in an array
        propArr1.push(prop1);
        valArr1.push(object_1[prop1]);                                              //Pushing thevalues in another array
    }

    for (prop2 in object_2) {
        propArr2.push(prop2);
        valArr2.push(object_2[prop2]);
    }
    propArr1 = propArr1.sort();                                                     //Sorting the properties and values
    propArr2 = propArr2.sort();
    valArr1 = valArr1.sort();
    valArr2 = valArr2.sort();
    
    for (i = 0; i < propArr1.length; i += 1) {                                      
        if (propArr1[i] !== propArr2[i] || valArr1[i] !== valArr2[i]) {
            return false;
        }
    }                                                                               //Checking if properties and values of both objects are same
    
    return true;

}

var Student = function () {                                                         //Creating a constructor
};
 
var student1 = new Student();                                                       //Creating Object 1
var student2 = new Student();                                                       //Creating object2


function main() {
    var property1 = document.getElementById("Property1").value;
    var property2 = document.getElementById("Property2").value;
    var property3 = document.getElementById("Property3").value;
    var property_1 = document.getElementById("property1").value;
    var property_2 = document.getElementById("property2").value;
    var property_3 = document.getElementById("property3").value;
    var value1 = document.getElementById("Value1").value;
    var value2 = document.getElementById("Value2").value;
    var value3 = document.getElementById("Value3").value;
    var value_1 = document.getElementById("value1").value;
    var value_2 = document.getElementById("value2").value;
    var value_3 = document.getElementById("value3").value;                         //Getting elements from html form

    student1[property1] = value1;                                                  //Assigning it to JavaScript variables
    student1[property2] = value2;
    student1[property3] = value3;
    student2[property_1] = value_1;
    student2[property_2] = value_2;
    student2[property_3] = value_3;

    var result = equalityCheck(student1, student2);                                 //Calling the function

    if (result === true) {
        alert("Two objects are same");                                              //Logging it to the user
    } else {
        alert("Two objects are not same");
    }
}